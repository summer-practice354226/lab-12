﻿#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;

int countVowels(const string& word) {
    int count = 0;
    for (char c : word) {
        if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y' || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' || c == 'Y') {
            count++;
        }
    }
    return count;
}

bool compareByVowelRatio(const pair<string, double>& a, const pair<string, double>& b) {
    return a.second < b.second;
}

int main() {
    string inputFile, outputFile;
    cout << "Enter the name of the input file: ";
    getline(cin, inputFile);
    cout << "Enter the name of the output file: ";
    getline(cin, outputFile);

    ifstream inFile(inputFile);
    ofstream outFile(outputFile);

    if (!inFile.is_open()) {
        cout << "Unable to open the input file." << endl;
        return 1;
    }

    if (!outFile.is_open()) {
        cout << "Unable to create the output file." << endl;
        inFile.close();
        return 1;
    }

    string text;
    string line;
    while (getline(inFile, line)) {
        text += line + "\n";
    }

    const int MAX_WORDS = 1024;
    string words[MAX_WORDS];
    int wordCount = 0;
    string word;
    for (char c : text) {
        if (c == ' ' || c == '\n') {
            if (!word.empty()) {
                words[wordCount++] = word;
                word.clear();
            }
        }
        else {
            word += c;
        }
    }
    if (!word.empty()) {
        words[wordCount++] = word;
    }

    pair<string, double> wordVowelRatios[MAX_WORDS];
    for (int i = 0; i < wordCount; i++) {
        int totalChars = static_cast<int>(words[i].length());
        int vowelCount = countVowels(words[i]);
        double vowelRatio = static_cast<double>(vowelCount) / totalChars;
        wordVowelRatios[i] = { words[i], vowelRatio };
    }

    sort(wordVowelRatios, wordVowelRatios + wordCount, compareByVowelRatio);

    for (int i = 0; i < wordCount; i++) {
        outFile << wordVowelRatios[i].first << " - " << wordVowelRatios[i].first << "\n";
    }

    inFile.close();
    outFile.close();

    cout << "Text processing complete. Output file generated." << endl;

    return 0;
}